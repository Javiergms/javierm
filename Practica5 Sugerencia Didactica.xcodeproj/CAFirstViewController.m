//
//  CAFirstViewController.m
//  Practica5 Sugerencia Didactica
//
//  Created by Walos on 10/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import "CAFirstViewController.h"

@interface CAFirstViewController ()

@end

@implementation CAFirstViewController
NSArray *estados;
int estadoSeleccionado=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    estados=[NSArray arrayWithObjects:[NSArray arrayWithObjects:@"sinaloa",@"Los mochis",@"Culiacan",@"Mazatlan", nil],[NSArray arrayWithObjects:@"Sonora",@"Obregon",@"Hermosillo", @"Nogales",nil],[NSArray arrayWithObjects:@"Chihuahua",@"Chihuahua",@"Parral",@"Ciudad juarez", nil], nil];

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    estados = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark pickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2; //Se indica que el pickerView Tendra 2 columnas
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0)
        return [estados count]; //regresa el numero de estados registrados
    return [[estados objectAtIndex:estadoSeleccionado]count]-1;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == 0)//Si cambian de estado
        if (estadoSeleccionado != row) {
            estadoSeleccionado = row;
            [pickerView reloadComponent:1];
        }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(component == 0)
        return [[estados objectAtIndex:row]objectAtIndex:0];
    return [[estados objectAtIndex:estadoSeleccionado]objectAtIndex:row+1];
}
@end
