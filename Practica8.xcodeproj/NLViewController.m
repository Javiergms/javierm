//
//  NLViewController.m
//  Practica8
//
//  Created by Walos on 14/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController
@synthesize texto;




- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.texto.text = [self leerDatos];
}

- (void)viewDidUnload
{
    [self setTexto:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


-(NSString *)leerDatos{
    NSString *directorio = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
    NSString *rutaCompleta = [directorio stringByAppendingPathComponent:@"archivo"];
    
    NSStringEncoding codificacion = NSUTF8StringEncoding;
    NSError *error;
    
    NSString *textoLeido = [NSString stringWithContentsOfFile:rutaCompleta encoding:codificacion error:&error];
    if(textoLeido == nil){
        textoLeido = @"No inicializado";
        return textoLeido;
    }
}

@end
