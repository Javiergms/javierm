//
//  MOTableViewControllerAlumnos.h
//  Practica6
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MODetalleViewController.h"
@interface MOTableViewControllerAlumnos : UITableViewController<AlumnoDetallesViewControllerDelegate>

@end
