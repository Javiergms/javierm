//
//  MODetalleViewController.h
//  Practica6
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MODetalleViewController;
@protocol AlumnoDetallesViewControllerDelegate <NSObject>

-(void)AlumnoDetallesViewControllerDelegateDidSave:(MODetalleViewController *)controller;

-(void)AlumnoDetallesViewControllerDelegateDidCancel:(MODetalleViewController *)controller;

@end

@interface MODetalleViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITextField *nombre;
@property (nonatomic, weak) id <AlumnoDetallesViewControllerDelegate> delegate;
- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end
