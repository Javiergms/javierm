//
//  MOAppDelegate.h
//  Practica6
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
