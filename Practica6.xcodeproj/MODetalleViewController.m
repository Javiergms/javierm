//
//  MODetalleViewController.m
//  Practica6
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import "MODetalleViewController.h"

@interface MODetalleViewController ()

@end

@implementation MODetalleViewController
@synthesize nombre;
@synthesize delegate;

-(IBAction)done:(id)sender{
    [self.delegate AlumnoDetallesViewControllerDelegateDidSave:self];
}

-(IBAction)cancel:(id)sender{
    [self.delegate AlumnoDetallesViewControllerDelegateDidCancel:self];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setNombre:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


@end
