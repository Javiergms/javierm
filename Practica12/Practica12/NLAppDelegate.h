//
//  NLAppDelegate.h
//  Practica12
//
//  Created by Patlan on 02/04/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
