//
//  JMAppDelegate.h
//  BluePrintsJavier
//
//  Created by Walos on 11/04/14.
//  Copyright (c) 2014 javier martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
