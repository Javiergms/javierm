//
//  dado.h
//  BluePrintsJavier
//
//  Created by Walos on 11/04/14.
//  Copyright (c) 2014 javier martinez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dado : NSObject
@property int numeroDados;
@property NSString* material;
-(int)asignarNumero;
-(NSString*)asignarMaterial;
@end
