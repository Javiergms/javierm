//
//  dado.m
//  BluePrintsJavier
//
//  Created by Walos on 11/04/14.
//  Copyright (c) 2014 javier martinez. All rights reserved.
//

#import "dado.h"

@implementation dado
@synthesize material;
@synthesize numeroDados;

-(int)asignarNumero{
    
    return 1 + arc4random() % (6 - 1 + 1);

}

-(NSString*)asignarMaterial{
    int m = 1 + arc4random() % (4 - 1 + 1);
    if (m == 1) {
        return @"VIDRIO";
    }else if (m == 2){
        return @"MADERA";
    }else if (m == 3){
        return @"PIEDRA";
    }else if (m == 4){
        return @"RECICLABLE";
    }

}

@end
