//
//  JMViewController.m
//  BluePrintsJavier
//
//  Created by Walos on 11/04/14.
//  Copyright (c) 2014 javier martinez. All rights reserved.
//

#import "JMViewController.h"

@interface JMViewController ()

@end

@implementation JMViewController
int jugadores = 4;
int v,p,m,r;
@synthesize bolsa;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)generarDado:(id)sender {
    //generar la bolsa
    bolsa = [[NSMutableArray alloc]initWithCapacity:32];
    //agregar 32 dados
    for (int a=0; a < 32; a++) {
        while (v<8) {
            dado *d = [[dado alloc]init];
            d.numeroDados = [d asignarNumero];
            d.material = [d asignarMaterial];
            if ([d.material isEqualToString:@"VIDRIO"]) {
                [bolsa addObject:d];
                v++;
            }
        }
        
        while (r<8) {
            dado *d = [[dado alloc]init];
            d.numeroDados = [d asignarNumero];
            d.material = [d asignarMaterial];
            if ([d.material isEqualToString:@"RECICLABLE"]) {
                [bolsa addObject:d];
                r++;
            }
        }
        
        while (p<8) {
            dado *d = [[dado alloc]init];
            d.numeroDados = [d asignarNumero];
            d.material = [d asignarMaterial];
            if ([d.material isEqualToString:@"PIEDRA"]) {
                [bolsa addObject:d];
                p++;
            }
        }
        
        while (m<8) {
            dado *d = [[dado alloc]init];
            d.numeroDados = [d asignarNumero];
            d.material = [d asignarMaterial];
            if ([d.material isEqualToString:@"MADERA"]) {
                [bolsa addObject:d];
                m++;
            }
        }
        
    }
    [self revolverDados];
    [self imprimirBolsa];
}
//Imprimir la bolsa
-(void)imprimirBolsa{
    for (int d=0; d<32; d++) {
        dado *dadoImprimir = [bolsa objectAtIndex:d];
        NSLog(@"%d  dado :%d material:%@",d,dadoImprimir.numeroDados,dadoImprimir.material);
    }
}
//Revolver la bolsa de dados
-(void)revolverDados{
    //contar el numero de dados en la bolsa
    NSUInteger medida = [bolsa count];
    for (NSUInteger i=0; i<medida; i++) {
        int elementosN = medida -i;
        int n = (arc4random() % elementosN) + i;
        [bolsa exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
}
@end
