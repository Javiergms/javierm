//
//  JMViewController.h
//  BluePrintsJavier
//
//  Created by Walos on 11/04/14.
//  Copyright (c) 2014 javier martinez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "dado.h"

@interface JMViewController : UIViewController{
    NSMutableArray *bolsa;
}
- (IBAction)generarDado:(id)sender;
-(void)imprimirBolsa;
-(void)revolverDados;
@property NSMutableArray *bolsa;
@end
