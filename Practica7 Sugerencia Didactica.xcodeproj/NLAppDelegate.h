//
//  NLAppDelegate.h
//  Practica7 Sugerencia Didactica
//
//  Created by Walos on 19/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
