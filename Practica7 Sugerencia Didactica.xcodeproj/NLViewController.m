//
//  NLViewController.m
//  Practica7 Sugerencia Didactica
//
//  Created by Walos on 19/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)notificar:(id)sender {
    NSDate *alertTime = [[NSDate date]dateByAddingTimeInterval:3];
    UIApplication * app =[UIApplication sharedApplication];
    UILocalNotification * notifyAlarm = [[UILocalNotification alloc]init];
    if (notifyAlarm) {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        [notifyAlarm setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        notifyAlarm.soundName = @"Glass.aiff";
        if ([sender tag]== 0) {
          notifyAlarm.alertBody = @"mensaje";  
        }else{
        notifyAlarm.alertBody = @"llamada";  
        }
            
        
        [app scheduleLocalNotification:notifyAlarm];
    }

}
@end
