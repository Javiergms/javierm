//
//  JMViewController.h
//  Practica 4 Sugerencia Didactica 1
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 Javier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
- (IBAction)procesarInformacion:(id)sender;

@end
