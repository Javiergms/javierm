//
//  JMViewController.m
//  Practica 4 Sugerencia Didactica 1
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 Javier. All rights reserved.
//

#import "JMViewController.h"

@interface JMViewController ()

@end

@implementation JMViewController
@synthesize texto;
@synthesize etiqueta;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setTexto:nil];
    [self setEtiqueta:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)procesarInformacion:(id)sender {
    NSUInteger longitud = [texto.text length];
    NSMutableString * rtr = [NSMutableString stringWithCapacity:longitud];
    while (longitud > (NSUInteger)0) {
        unichar uch = [texto.text characterAtIndex:--longitud];
        [rtr appendString:[NSString stringWithCharacters:&uch length:1]];
    }
    etiqueta.text=rtr;
    [texto resignFirstResponder];
}
@end
