Nombre: Javier Gerardo Martinez Salomon   NO.10440992

Explicar porque la celda de agregar alumno se pone Azul cuando se hace un tap en los extremos.

Despu�s de analizar y manipular algunas propiedades de la celda y el text field, llegu� a la conclusi�n de que la celda se pone azul debido a que es seleccionada y en las propiedades en el atributes inspector �sta se tiene configurada para que al ser seleccionada cambie de color a azul, si se cambia esta opci�n a NONE , la celda no se selecciona (o no produce un cambio significativo almenos) con lo cual la celda no cambia de color.