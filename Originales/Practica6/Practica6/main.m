//
//  main.m
//  Practica6
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}
