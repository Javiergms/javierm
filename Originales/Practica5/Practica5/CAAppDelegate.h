//
//  CAAppDelegate.h
//  Practica5
//
//  Created by Walos on 10/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
