//
//  CACiudadesViewController.h
//  Practica5
//
//  Created by Walos on 10/03/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CACiudadesViewController : UITableViewController
@property NSArray *ciudades;
@end
