//
//  ProxyBD.m
//  Practica9
//
//  Created by Walos on 07/04/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import "ProxyBD.h"
#include <sqlite3.h>

@implementation ProxyBD

-(NSMutableArray *)nombres{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    
    const char *consulta = "Select nombre from informacion";
    
    NSString *ruta =[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    
    sqlite3_open([ruta UTF8String], &laBd);
    
    sqlite3_prepare(laBd, consulta, -1, &consultaPreparada, NULL);
    
    NSMutableArray *resultados = [[NSMutableArray alloc]init];
    
    while (sqlite3_step(consultaPreparada)==SQLITE_ROW) {
        [resultados addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(consultaPreparada, 0)]];
    }
    sqlite3_close(laBd);
    return resultados;

}

-(void)insertarNombre:(NSString *)nombre{
    sqlite3 *laBd;
    sqlite3_stmt *consultaPreparada;
    const char *consulta = "Insert into informacion(nombre) values(?)";
    
    NSString *ruta = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"mibd"];
    
    sqlite3_open([ruta UTF8String], &laBd);
    
    sqlite3_prepare(laBd, consulta, -1, &consultaPreparada, NULL);
    sqlite3_bind_text(consultaPreparada, 1, [nombre UTF8String], -1, SQLITE_TRANSIENT);
    
    sqlite3_step(consultaPreparada);
    sqlite3_finalize(consultaPreparada);
    sqlite3_close(laBd);
}
@end
