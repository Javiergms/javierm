//
//  NLViewController.h
//  Practica9
//
//  Created by Walos on 07/04/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProxyBD.h"

@interface NLViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)oprimir:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tabla;
@property (strong, nonatomic) ProxyBD *proxyBD;
@end
