//
//  ProxyBD.h
//  Practica9
//
//  Created by Walos on 07/04/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProxyBD : NSObject
-(NSMutableArray *)nombres;
-(void)insertarNombre:(NSString *)nombre;
@end
