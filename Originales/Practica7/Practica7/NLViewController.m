//
//  NLViewController.m
//  Practica7
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)notificar:(id)sender {
    NSDate *alertTime = [[NSDate date]dateByAddingTimeInterval:10];
    UIApplication * app =[UIApplication sharedApplication];
    UILocalNotification * notifyAlarm = [[UILocalNotification alloc]init];
    if (notifyAlarm) {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        [notifyAlarm setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        notifyAlarm.soundName = @"Glass.aiff";
        notifyAlarm.alertBody = @"Esta es una notificacion";
        [app scheduleLocalNotification:notifyAlarm];
    }
}
@end
