//
//  NLAppDelegate.h
//  Practica7
//
//  Created by Walos on 11/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
