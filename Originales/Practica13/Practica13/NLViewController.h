//
//  NLViewController.h
//  Practica13
//
//  Created by Patlan on 10/04/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *texto;
- (IBAction)escribirCodigo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *temperatura;
@property (weak, nonatomic) IBOutlet UILabel *presion;
@property (weak, nonatomic) IBOutlet UILabel *poblacion;

@end
