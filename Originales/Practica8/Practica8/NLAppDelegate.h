//
//  NLAppDelegate.h
//  Practica8
//
//  Created by Walos on 14/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
