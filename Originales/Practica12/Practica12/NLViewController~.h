//
//  NLViewController.h
//  Practica12
//
//  Created by Patlan on 02/04/14.
//  Copyright (c) 2014 JavierMartinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *texto;
- (IBAction)terminarEscribir:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *etiqueta;
@property (strong, nonatomic) NSMutableData *datosWeb;
@end
