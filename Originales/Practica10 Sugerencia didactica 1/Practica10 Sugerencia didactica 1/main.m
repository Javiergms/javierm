//
//  main.m
//  Practica10 Sugerencia didactica 1
//
//  Created by Patlan on 26/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLAppDelegate class]));
    }
}
