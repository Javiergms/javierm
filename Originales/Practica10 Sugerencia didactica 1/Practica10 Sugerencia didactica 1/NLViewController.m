//
//  NLViewController.m
//  Practica10 Sugerencia didactica 1
//
//  Created by Patlan on 26/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

NSString *direccion;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)navegar:(id)sender {
    
    switch ([sender tag]) {
        case 0:
          direccion = @"http://www.google.com.mx";
            break;
            
        case 1:
            direccion = @"http://www.ebay.com";
            break;
            
        case 2:
            direccion = @"http://www.9gag.com";
            break;
            
        case 3:
            direccion = @"http://www.youtube.com";
            break;
            
        case 4:
            direccion = @"http://www.stackoverflow.com";
            break;
    }
    NSURL *url = [NSURL URLWithString:direccion];
    
    NSURLRequest *peticion = [NSURLRequest requestWithURL:url];
    
    [self.navegador loadRequest:peticion];
    
}
@end
