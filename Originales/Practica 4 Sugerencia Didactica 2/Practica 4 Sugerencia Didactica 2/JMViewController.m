//
//  JMViewController.m
//  Practica 4 Sugerencia Didactica 2
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 Javier. All rights reserved.
//

#import "JMViewController.h"

@interface JMViewController ()

@end

@implementation JMViewController
@synthesize display;
int numeroActual;
int operacion = 0;
int resultado;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setDisplay:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)digitoPresionado:(UIButton *)sender {
    numeroActual = numeroActual *10 + sender.tag;
    display.text = [NSString stringWithFormat:@"%2i", numeroActual];
          
    
}

- (IBAction)operacion:(UIButton *)sender {
    if (operacion == 0) {
        resultado = numeroActual;
    }else{
        if (operacion == 1) {
            resultado = resultado + numeroActual;
            
        }
        if (operacion == 2) {
            resultado = resultado - numeroActual;
            
        }
        if (operacion == 3) {
            resultado = resultado * numeroActual;
            
        }
        if (operacion == 4) {
            resultado = resultado / numeroActual;
            
        }
        if (operacion == 5) {
            operacion = 0;
        }
        }

    numeroActual = 0;
    display.text = [NSString stringWithFormat:@"%2i",resultado];
    if (sender.tag == 0) {
        resultado = 0;

    }
    operacion = sender.tag;
}
@end
