//
//  JMViewController.h
//  Practica 4 Sugerencia Didactica 2
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 Javier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *display;
- (IBAction)digitoPresionado:(id)sender;
- (IBAction)operacion:(id)sender;

@end
