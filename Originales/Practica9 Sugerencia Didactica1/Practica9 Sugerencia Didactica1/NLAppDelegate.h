//
//  NLAppDelegate.h
//  Practica9 Sugerencia Didactica1
//
//  Created by Walos on 08/04/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
