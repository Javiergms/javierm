//
//  ProxyBD.h
//  Practica9 Sugerencia Didactica1
//
//  Created by Walos on 08/04/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProxyBD : NSObject
-(NSMutableArray *)nombres;
-(void)insertarNombre:(NSString *)nombre;
-(void)deleteName;
@end
