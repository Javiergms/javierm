//
//  NLAppDelegate.h
//  Practica11 Sugerencia Didactica1
//
//  Created by Patlan on 27/03/14.
//  Copyright (c) 2014 Javier Martinez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
