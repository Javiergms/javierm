Alumno: Mart�nez Salom�n Javier Gerardo   NO.10440992
Comparaci�n del IDE de Android SDK con Xcode
1-	Completador de Texto
En el IDE de android el completador de texto cumple su funci�n de completar y auto predecir lo que el programador quiere anotar, sin embargo si el usuario se sale de una escritura de c�digo v�lida, el completador deja de funcionar, por lo cual debe borrarse la l�nea de c�digo y empezar de nuevo para habilitarlo. En contraste el completador de Xcode es muy efectivo, pues predice con total certeza lo que el usuario quiere, completando de forma eficaz,  a�n cuando el usuario introduce una sentencia incorrecta.
Este punto es MEJOR en Xcode que en android IDE.
2-	Emulador de dispositivo
En Xcode el emulador del dispositivo es sumamente eficaz, fluido y r�pido. A diferencia, en android, el emulador del dispositivo debe ser instalado desde cero cada vez que se utiliza el IDE por lo cual tarda un tiempo considerable en ejecutarse y en probar la aplicaci�n.
Este punto es MEJOR en Xcode que en android IDE
3-	Prueba de aplicaciones F�sicas
En Xcode es posible emular las aplicaciones en dispositivos f�sicos Iphone/Ipad, pero para esto, es necesario, adem�s de contar con un dispositivo real, contar con una clave de autorizaci�n que te certifique como desarrollador de apps. En contraste, la IDE de android es capaz de emular aplicaciones en dispositivos f�sicos solamente con instalar el driver USB del dispositivo. 
Este punto es PEOR en Xcode que en android IDE
