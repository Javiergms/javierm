//
//  main.m
//  Practica3 Sugerencia
//
//  Created by Walos on 04/03/14.
//  Copyright (c) 2014 Javier. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JMAppDelegate class]));
    }
}
