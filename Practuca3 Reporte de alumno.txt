Alumno: Martinez Salomon Javier Gerardo   No.10440992

Existe una diferencia significativa en posici�n y tama�o de los componentes entre una de las orientaciones y las otras
3, indique cual y explique la razon de esa diferencia, tambi�n indique en que cambia la forma de responder los controles cuando se aplica esa orientaci�n.

En la primera corrida, tanto el button como el textfield, se salen de los m�rgenes de la pantalla, esto es porque
ambos en el size inspector tienen anclados tanto a la izquierda como arriba mediante struts, las acciones ejecutadas por 
los controles no cambian, sin embargo, se vuelven inaccesibles, por tanto no se puede operar con ellos,sin embargo al desanclar y establecer las formas en las cuales los controles pueden crecer y mantenerse, estos mantienen su posici�n en cualquier orientaci�n evitando esconder los controles fuera de los rangos de la pantalla.

